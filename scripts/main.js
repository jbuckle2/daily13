console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var name = document.getElementById("name-text").value;
    console.log('Name you entered is ' + name);
    makeNetworkCallToNationalityApi(name);

} // end of get form info

function makeNetworkCallToNationalityApi(name){
    console.log('entered make nw call' + name);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.nationalize.io/?name=" + name;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateNationalityWithResponse(name, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateNationalityWithResponse(name, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line1");

    var countryList = response_json['country'];
    var countryDict = countryList[0];
    var country = countryDict['country_id'];

    if(country == null){
        label1.innerHTML = 'Apologies, we could not find your name.'
    } else{
        label1.innerHTML =  name + ', the country code of your nationality is ' + country;
        makeNetworkCallToFlag(country);
    }
} // end of updateAgeWithResponse

function makeNetworkCallToFlag(country){
    console.log('entered make nw call' + country);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://restcountries.eu/rest/v2/alpha/" + country;
    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateCountryWithResponse(country, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateCountryWithResponse(country, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label2 = document.getElementById("response-line2");
    label2.innerHTML = 'The country name is ' + response_json['name'];

} // end of updateTriviaWithResponse
